"use strict";

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface
            .createTable('available_tables', {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false
                },
                restaurantID: Sequelize.INTEGER,
                reservationDate: Sequelize.DATE,
                twoSeatTables: Sequelize.INTEGER,
                fourSeatTables: Sequelize.INTEGER,
                sixSeatTables: Sequelize.INTEGER,
                createdAt: {
                    type: Sequelize.DATE,
                    allowNull: false
                },
                updatedAt: Sequelize.DATE
            });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface
            .dropTable('available_tables');
    }
};
