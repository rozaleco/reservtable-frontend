"use strict";

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface
            .createTable('user', {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false
                },
                email: {type: Sequelize.STRING, unique: true},
                password: Sequelize.STRING,
                phoneNumber: Sequelize.STRING,
                firstName: Sequelize.STRING,
                lastName: Sequelize.STRING,
                createdAt: {
                    type: Sequelize.DATE,
                    allowNull: false
                },
                updatedAt: Sequelize.DATE
            });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface
            .dropTable('user');
    }
};
